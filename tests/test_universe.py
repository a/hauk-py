import pytest

pytestmark = pytest.mark.asyncio


def test_universe():
    assert 1 == 1


async def test_index(test_cli):
    resp = await test_cli.get("/")
    assert resp.status_code == 200
