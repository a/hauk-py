import time
import pytest

pytestmark = pytest.mark.asyncio


async def test_hauk_basic_flow(test_cli):
    resp = await test_cli.post(
        "/api/create.php",
        form={
            "dur": "10",
            "int": "10",
            "pwd": "",
        },
    )
    assert resp.status_code == 200
    response_data = (await resp.data).decode()
    ok, session_id, _view_link, session_name = response_data.split("\n")
    assert ok == "OK"

    # generate 5 datapoints at different times, so we can validate
    # ranged fetches

    for num in range(5):
        resp = await test_cli.post(
            "/api/post.php",
            form={
                "sid": session_id,
                "lat": str(num),
                "lon": "0",
                "acc": "0",
                "time": time.time() + num,
            },
        )
        assert resp.status_code == 200
        response_data = (await resp.data).decode()
        responses = response_data.split("\n")
        assert len(responses) == 4
        assert responses[0] == "OK"

    resp = await test_cli.get(
        "/api/fetch.php",
        query_string={
            "id": session_name,
        },
    )
    assert resp.status_code == 200
    response_data = await resp.json
    assert response_data["type"] == 0
    assert response_data["serverTime"]

    points = response_data["points"]
    assert len(points) == 5
    wanted_slice = points[2:]
    first_from_slice = wanted_slice[0]

    resp = await test_cli.get(
        "/api/fetch.php",
        query_string={
            "id": session_name,
            "since": str(first_from_slice[2]),
        },
    )
    assert resp.status_code == 200
    response_data = await resp.json
    new_points = response_data["points"]
    assert len(new_points) == 3

    # TODO add delete test


async def test_hauk_create_invalid_password(test_cli):
    resp = await test_cli.post(
        "/api/create.php",
        form={
            "dur": "10",
            "int": "10",
            "pwd": "awooga",
        },
    )
    assert resp.status_code == 200
    response_data = (await resp.data).decode()
    assert response_data.startswith("Incorrect password.")


async def test_hauk_e2e(test_cli):
    resp = await test_cli.post(
        "/api/create.php",
        form={"dur": "10", "int": "10", "pwd": "", "e2e": True, "salt": "test123"},
    )
    assert resp.status_code == 200
    response_data = (await resp.data).decode()
    ok, session_id, _view_link, session_name = response_data.split("\n")
    assert ok == "OK"

    for num in range(5):
        resp = await test_cli.post(
            "/api/post.php",
            form={
                "sid": session_id,
                "lat": "0",
                "lon": "0",
                "acc": "0",
                "spd": "0",
                "prv": "0",
                "time": "0",
                "iv": f"test{num}",
            },
        )
        assert resp.status_code == 200
        response_data = (await resp.data).decode()
        responses = response_data.split("\n")
        assert len(responses) == 4
        assert responses[0] == "OK"

    resp = await test_cli.get(
        "/api/fetch.php",
        query_string={
            "id": session_name,
        },
    )
    assert resp.status_code == 200
    response_data = await resp.json

    points = response_data["points"]
    assert len(points) == 5
    for point in points:
        assert point[0].startswith("test")
